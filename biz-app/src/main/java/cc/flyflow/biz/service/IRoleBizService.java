package cc.flyflow.biz.service;

import cc.flyflow.common.dto.R;
import cc.flyflow.common.dto.flow.NodeUser;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author Vincent
 * @since 2023-06-08
 */
public interface IRoleBizService  {





    /**
     * 查询所有角色
     * @return
     */
    R queryAll();



    /**
     * 保存角色用户
     * @param nodeUserDtoList
     * @param id
     * @return
     */
    R saveUserList(List<NodeUser> nodeUserDtoList, String id);


}
