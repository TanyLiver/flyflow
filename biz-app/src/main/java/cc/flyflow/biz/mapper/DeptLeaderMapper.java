package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.DeptLeader;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * <p>
 * 部门主管表 Mapper 接口
 * </p>
 *
 * @author xiaoge
 * @since 2023-05-05
 */
public interface DeptLeaderMapper extends MPJBaseMapper<DeptLeader> {



}
