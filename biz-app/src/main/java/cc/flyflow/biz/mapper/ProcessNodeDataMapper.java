package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.ProcessNodeData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 流程节点数据 Mapper 接口
 * </p>
 *
 * @author Vincent
 * @since 2023-05-07
 */
public interface ProcessNodeDataMapper extends BaseMapper<ProcessNodeData> {

}
