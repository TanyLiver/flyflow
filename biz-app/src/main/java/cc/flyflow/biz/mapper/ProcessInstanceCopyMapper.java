package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.ProcessInstanceCopy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 流程抄送数据 Mapper 接口
 * </p>
 *
 * @author Vincent
 * @since 2023-05-20
 */
public interface ProcessInstanceCopyMapper extends BaseMapper<ProcessInstanceCopy> {

}
