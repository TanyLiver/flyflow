package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author Vincent
 * @since 2023-06-08
 */
public interface RoleMapper extends BaseMapper<Role> {

}
