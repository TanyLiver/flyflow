package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.User;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author xiaoge
 * @since 2023-05-05
 */
public interface UserMapper extends MPJBaseMapper<User> {

}
