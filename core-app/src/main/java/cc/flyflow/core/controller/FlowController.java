package cc.flyflow.core.controller;

import cc.flyflow.common.dto.*;
import cc.flyflow.core.service.IFlowService;
import cc.flyflow.core.utils.FlowableUtils;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import cc.flyflow.common.config.NotWriteLogAnno;
import cc.flyflow.common.constants.ProcessInstanceConstant;
import cc.flyflow.common.utils.TenantUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.BpmnAutoLayout;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.history.HistoricProcessInstanceQuery;
import org.flowable.engine.impl.persistence.entity.HistoricProcessInstanceEntityImpl;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.image.impl.DefaultProcessDiagramGenerator;
import org.flowable.task.api.Task;
import org.flowable.task.api.TaskQuery;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.task.api.history.HistoricTaskInstanceQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 工作流控制器
 * 负责流程创建编辑发起等功能
 */
@Api(value = "flow", tags = {"工作流控制器 负责流程创建编辑发起等功能"})
@RestController
@Slf4j
@RequestMapping("flow")
public class FlowController {

    @Autowired
    private TaskService taskService;
    @Resource
    private HistoryService historyService;
    @Resource
    private RepositoryService repositoryService;


    @Resource
    private RuntimeService runtimeService;
    @Resource
    private IFlowService flowService;

    /**
     * 创建流程
     *
     * @param createFlowDto
     * @return flowId流程id
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "CreateFlowDto", name = "createFlowDto", value = "", required = true)
    })
    @ApiOperation(value = "创建流程", notes = "创建流程", httpMethod = "POST")
    @PostMapping("create")
    public R<String> create(@RequestBody CreateFlowDto createFlowDto) {
        return flowService.create(createFlowDto);
    }

    /**
     * 启动流程
     *
     * @param processInstanceParamDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "ProcessInstanceParamDto", name = "processInstanceParamDto", value = "", required = true)
    })
    @ApiOperation(value = "启动流程", notes = "启动流程", httpMethod = "POST")
    @PostMapping("/start")
    public R start(@RequestBody ProcessInstanceParamDto processInstanceParamDto) {
        return flowService.start(processInstanceParamDto);


    }


    /**
     * 清理所有的流程
     *
     * @param clearProcessParamDto 清理数据对象
     * @return 成功失败
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "ClearProcessParamDto", name = "clearProcessParamDto", value = "清理数据对象", required = true)
    })
    @ApiOperation(value = "清理所有的流程", notes = "清理所有的流程", httpMethod = "POST")
    @PostMapping("clearProcess")
    public R clearProcess(@RequestBody ClearProcessParamDto clearProcessParamDto) {
        return flowService.clearProcess(clearProcessParamDto);
    }

    /**
     * 查看流程实例执行图片
     *
     * @param processInstanceId
     * @param response
     */
    @ApiOperation(value = "查看流程实例执行图片", notes = "查看流程实例执行图片", httpMethod = "GET")
    @NotWriteLogAnno(all = true, printResultLog = false)
    @GetMapping("/showImg")
    @SneakyThrows
    public void showImg(String processInstanceId, HttpServletResponse response) {


        String procDefId;
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        if (processInstance == null) {
            HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                    .processInstanceId(processInstanceId)
                    .singleResult();
            procDefId = historicProcessInstance.getProcessDefinitionId();

        } else {
            procDefId = processInstance.getProcessDefinitionId();
        }

        //使用流程实例ID，查询正在执行的执行对象表，返回流程实例对象
        BpmnModel bpmnModel = repositoryService.getBpmnModel(procDefId);


        // 创建默认的流程图生成器
        DefaultProcessDiagramGenerator defaultProcessDiagramGenerator = new DefaultProcessDiagramGenerator();
        // 生成图片的类型
        String imageType = "png";
        // 高亮节点集合
        List<String> highLightedActivities = new ArrayList<>();
        // 高亮连线集合
        List<String> highLightedFlows = new ArrayList<>();
        // 查询所有历史节点信息
        List<HistoricActivityInstance> hisActInsList = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId)
                .list();


        // 遍历
        hisActInsList.forEach(historicActivityInstance -> {
            if ("sequenceFlow".equals(historicActivityInstance.getActivityType())) {
                // 添加高亮连线
                highLightedFlows.add(historicActivityInstance.getActivityId());
            } else {
                // 添加高亮节点
                highLightedActivities.add(historicActivityInstance.getActivityId());
            }
        });
        // 节点字体
        String activityFontName = "宋体";
        // 连线标签字体
        String labelFontName = "微软雅黑";
        // 连线标签字体
        String annotationFontName = "宋体";
        // 类加载器
        ClassLoader customClassLoader = null;
        // 比例因子，默认即可
        double scaleFactor = 1.0d;
        // 不设置连线标签不会画
        boolean drawSequenceFlowNameWithNoLabelDI = true;

        BpmnAutoLayout bpmnAutoLayout = new BpmnAutoLayout(bpmnModel);
        bpmnAutoLayout.setTaskHeight(120);
        bpmnAutoLayout.setTaskWidth(120);
        bpmnAutoLayout.execute();
        // 生成图片
        InputStream inputStream = defaultProcessDiagramGenerator.generateDiagram(bpmnModel,
                imageType,
                highLightedActivities,
                highLightedFlows,
                activityFontName,
                labelFontName,
                annotationFontName,
                customClassLoader,
                scaleFactor,
                drawSequenceFlowNameWithNoLabelDI); // 获取输入流
        IoUtil.write(response.getOutputStream(), true, IoUtil.readBytes(inputStream));
//        String content = Base64.encode(inputStream);
        // return R.success(content);
    }


    /**
     * 终止流程
     *
     * @param taskParamDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "TaskParamDto", name = "taskParamDto", value = "", required = true)
    })
    @ApiOperation(value = "终止流程", notes = "终止流程", httpMethod = "POST")
    @Deprecated
    @PostMapping("stopProcessInstance")
    public R stopProcessInstance(@RequestBody TaskParamDto taskParamDto) {

        List<String> processInstanceIdList = taskParamDto.getProcessInstanceIdList();
        for (String processInstanceId : processInstanceIdList) {
            ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
            if (processInstance != null) {
                runtimeService.setVariable(processInstanceId, ProcessInstanceConstant.VariableKey.CANCEL, true);
                List<Execution> executions = runtimeService.createExecutionQuery().parentId(processInstanceId).list();
                List<String> executionIds = new ArrayList<>();
                executions.forEach(execution -> executionIds.add(execution.getId()));
                runtimeService.createChangeActivityStateBuilder().moveExecutionsToSingleActivityId(executionIds,
                        ProcessInstanceConstant.VariableKey.END).changeState();
            }
        }


        return R.success();
    }

    /**
     * 获取我已办的流程实例
     *
     * @param processQueryParamDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "ProcessQueryParamDto", name = "processQueryParamDto", value = "", required = true)
    })
    @ApiOperation(value = "获取我已办的流程实例", notes = "获取我已办的流程实例", httpMethod = "POST")
    @PostMapping("queryCompletedProcessInstance")
    public R<PageResultDto<ProcessInstanceDto>> queryCompletedProcessInstance(@RequestBody ProcessQueryParamDto processQueryParamDto) {
        HistoricProcessInstanceQuery historicProcessInstanceQuery =
                historyService.createHistoricProcessInstanceQuery().processInstanceTenantId(TenantUtil.get());

        if (CollUtil.isNotEmpty(processQueryParamDto.getFlowIdList())) {
            historicProcessInstanceQuery = historicProcessInstanceQuery.processDefinitionKeyIn(processQueryParamDto.getFlowIdList());
        }

        List<HistoricProcessInstance> list = historicProcessInstanceQuery

                .involvedUser(processQueryParamDto.getAssign())
                .orderByProcessInstanceStartTime().desc()
                .listPage((processQueryParamDto.getPageNum() - 1) * processQueryParamDto.getPageSize(),
                        processQueryParamDto.getPageSize());

        long count = historicProcessInstanceQuery

                .involvedUser(processQueryParamDto.getAssign())
                .count();


        List<ProcessInstanceDto> processInstanceParamDtoList = new ArrayList<>();

        for (HistoricProcessInstance historicProcessInstance : list) {

            HistoricProcessInstanceEntityImpl historicProcessInstanceEntity = (HistoricProcessInstanceEntityImpl) historicProcessInstance;
            String processInstanceId = historicProcessInstanceEntity.getProcessInstanceId();
            String flowId = historicProcessInstanceEntity.getProcessDefinitionKey();
            String processName = historicProcessInstanceEntity.getProcessDefinitionName();

            ProcessInstanceDto processInstanceDto = new ProcessInstanceDto();
            processInstanceDto.setProcessInstanceId(processInstanceId);
            processInstanceDto.setFlowId(flowId);
            processInstanceDto.setProcessName(processName);
            processInstanceDto.setStartUserId(historicProcessInstance.getStartUserId());
            processInstanceDto.setStartTime(historicProcessInstance.getStartTime());
            processInstanceDto.setEndTime(historicProcessInstance.getEndTime());
            processInstanceParamDtoList.add(processInstanceDto);


        }
        PageResultDto<ProcessInstanceDto> pageResultDto = new PageResultDto<>();
        pageResultDto.setTotal(count);
        pageResultDto.setRecords(processInstanceParamDtoList);


        return R.success(pageResultDto);
    }

    /**
     * 查询用户已办任务
     *
     * @param taskQueryParamDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "TaskQueryParamDto", name = "taskQueryParamDto", value = "", required = true)
    })
    @ApiOperation(value = "查询用户已办任务", notes = "查询用户已办任务", httpMethod = "POST")
    @PostMapping("/queryCompletedTask")
    public R<PageResultDto<TaskDto>> queryCompletedTask(@RequestBody TaskQueryParamDto taskQueryParamDto) {
        HistoricTaskInstanceQuery historicTaskInstanceQuery = historyService.createHistoricTaskInstanceQuery().taskTenantId(TenantUtil.get());

        List<HistoricTaskInstance> list = historicTaskInstanceQuery
                .taskAssignee(taskQueryParamDto.getAssign())
                .taskWithoutDeleteReason()
                .finished()
                .orderByHistoricTaskInstanceEndTime().desc()
                .listPage((taskQueryParamDto.getPageNum() - 1) * taskQueryParamDto.getPageSize(),
                        taskQueryParamDto.getPageSize());

        long count = historicTaskInstanceQuery.taskAssignee(taskQueryParamDto.getAssign()).taskWithoutDeleteReason().finished().count();
        List<TaskDto> taskDtoList = new ArrayList<>();

        for (HistoricTaskInstance instance : list) {
            String activityId = instance.getTaskDefinitionKey();
            String activityName = instance.getName();
            String executionId = instance.getExecutionId();
            String taskId = instance.getId();
            Date startTime = instance.getStartTime();
            Date endTime = instance.getEndTime();
            Long durationInMillis = instance.getDurationInMillis();
            String processInstanceId = instance.getProcessInstanceId();

            String processDefinitionId = instance.getProcessDefinitionId();
            //流程id
            String flowId = FlowableUtils.getFlowId(processDefinitionId, TenantUtil.get());


            TaskDto taskDto = new TaskDto();
            taskDto.setFlowId(flowId);
            taskDto.setTaskCreateTime(startTime);
            taskDto.setTaskEndTime(endTime);
            taskDto.setNodeId(activityId);
            taskDto.setExecutionId(executionId);
            taskDto.setProcessInstanceId(processInstanceId);
            taskDto.setDurationInMillis(durationInMillis);
            taskDto.setTaskId(taskId);
            taskDto.setAssign(instance.getAssignee());
            taskDto.setTaskName(activityName);


            taskDtoList.add(taskDto);
        }
        PageResultDto<TaskDto> pageResultDto = new PageResultDto<>();
        pageResultDto.setTotal(count);
        pageResultDto.setRecords(taskDtoList);


        return R.success(pageResultDto);
    }


    /**
     * 查询任务的执行人
     *
     * @param taskParamDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "TaskParamDto", name = "taskParamDto", value = "", required = true)
    })
    @ApiOperation(value = "查询任务的执行人", notes = "查询任务的执行人", httpMethod = "POST")
    @PostMapping("/queryTaskAssignee")
    public R<List<TaskDto>> queryTaskAssignee(@RequestBody TaskParamDto taskParamDto) {

        TaskQuery taskQuery = taskService.createTaskQuery().taskTenantId(TenantUtil.get());
        if (StrUtil.isNotBlank(taskParamDto.getNodeId())) {
            taskQuery = taskQuery.taskDefinitionKey(taskParamDto.getNodeId());
        }
        List<Task> list =
                taskQuery.processInstanceId(taskParamDto.getProcessInstanceId()).list();


        List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : list) {
            TaskDto taskDto = new TaskDto();
            taskDto.setAssign(task.getAssignee());
            taskDto.setExecutionId(task.getExecutionId());
            taskDto.setTaskId(task.getId());
            taskDto.setTaskName(task.getName());
            taskDto.setNodeId(task.getTaskDefinitionKey());
            taskDto.setProcessInstanceId(task.getProcessInstanceId());

            String processDefinitionId = task.getProcessDefinitionId();
            //流程id
            String flowId = FlowableUtils.getFlowId(processDefinitionId, TenantUtil.get());

            taskDto.setFlowId(flowId);
            taskDtoList.add(taskDto);
        }


        return R.success(taskDtoList);
    }

    /**
     * 查询用户待办任务
     *
     * @param taskQueryParamDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "TaskQueryParamDto", name = "taskQueryParamDto", value = "", required = true)
    })
    @ApiOperation(value = "查询用户待办任务", notes = "查询用户待办任务", httpMethod = "POST")
    @PostMapping("/queryTodoTask")
    public R<PageResultDto<TaskDto>> queryTodoTask(@RequestBody TaskQueryParamDto taskQueryParamDto) {

        String assign = taskQueryParamDto.getAssign();

        TaskQuery taskQuery = taskService.createTaskQuery().taskTenantId(TenantUtil.get());


        List<Task> tasks =
                taskQuery.taskAssignee(assign).orderByTaskCreateTime().desc().listPage((taskQueryParamDto.getPageNum() - 1) * taskQueryParamDto.getPageSize(),
                        taskQueryParamDto.getPageSize());
        long count = taskQuery.taskAssignee(assign).count();

        List<TaskDto> taskDtoList = new ArrayList<>();
        log.debug("当前有" + count + " 个任务:");
        for (int i = 0; i < tasks.size(); i++) {
            Task task = tasks.get(i);
            String taskId = task.getId();
            String processInstanceId = task.getProcessInstanceId();
            log.debug((i + 1) + ") (" + taskId + ") " + task.getName() + " processInstanceId={} executrionId={}",
                    processInstanceId, task.getExecutionId());

            Map<String, Object> taskServiceVariables = taskService.getVariables(task.getId());
            log.debug("任务变量:{}", JSONUtil.toJsonStr(taskServiceVariables));


            //nodeid
            String taskDefinitionKey = task.getTaskDefinitionKey();

            String processDefinitionId = task.getProcessDefinitionId();
            //流程id
            String flowId = FlowableUtils.getFlowId(processDefinitionId, TenantUtil.get());


            TaskDto taskDto = new TaskDto();
            taskDto.setFlowId(flowId);
            taskDto.setTaskCreateTime(task.getCreateTime());
            taskDto.setNodeId(taskDefinitionKey);
            taskDto.setParamMap(taskServiceVariables);
            taskDto.setProcessInstanceId(processInstanceId);
            taskDto.setTaskId(taskId);
            taskDto.setAssign(task.getAssignee());
            taskDto.setTaskName(task.getName());
            taskDtoList.add(taskDto);

        }

        PageResultDto<TaskDto> pageResultDto = new PageResultDto<>();
        pageResultDto.setTotal(count);
        pageResultDto.setRecords(taskDtoList);


        return R.success(pageResultDto);
    }


}
