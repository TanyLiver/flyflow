package cc.flyflow.core.controller;

import cc.flyflow.common.dto.IndexPageStatistics;
import cc.flyflow.common.dto.R;
import cc.flyflow.common.dto.VariableQueryParamDto;
import cc.flyflow.common.utils.TenantUtil;
import cc.flyflow.core.service.IProcessInstanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstanceQuery;
import org.flowable.task.api.TaskQuery;
import org.flowable.variable.api.history.HistoricVariableInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程实例接口
 */
@Api(value = "process-instance", tags = {"流程实例接口"})
@RestController
@Slf4j
@RequestMapping("process-instance")
public class ProcessInstanceController {

    @Autowired
    private TaskService taskService;
    @Resource
    private HistoryService historyService;


    @Resource
    private RuntimeService runtimeService;


    @Resource
    private IProcessInstanceService processInstanceService;


    /**
     * 查询统计数量
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "查询统计数量", notes = "查询统计数量", httpMethod = "GET")
    @GetMapping("querySimpleData")
    public R<IndexPageStatistics> querySimpleData(String userId) {
        TaskQuery taskQuery = taskService.createTaskQuery().taskTenantId(TenantUtil.get());

        //待办数量
        long pendingNum = taskQuery.taskAssignee((userId)).count();
        //已完成任务
        HistoricActivityInstanceQuery historicActivityInstanceQuery =
                historyService.createHistoricActivityInstanceQuery().activityTenantId(TenantUtil.get());

        long completedNum = historicActivityInstanceQuery.taskAssignee(String.valueOf(userId)).finished().count();


        IndexPageStatistics indexPageStatistics = IndexPageStatistics.builder().pendingNum(pendingNum).completedNum(completedNum).build();

        return R.success(indexPageStatistics);
    }

    /**
     * 查询变量
     *
     * @param paramDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "VariableQueryParamDto", name = "paramDto", value = "", required = true)
    })
    @ApiOperation(value = "查询变量", notes = "查询变量", httpMethod = "POST")
    @PostMapping("queryVariables")
    public R<Map<String, Object>> queryVariables(@RequestBody VariableQueryParamDto paramDto) {

        long count =
                runtimeService.createProcessInstanceQuery().processInstanceTenantId(TenantUtil.get()).processInstanceId(paramDto.getExecutionId()).count();
        if(count>0){

            Map<String, Object> variables = runtimeService.getVariables(paramDto.getExecutionId());


            return R.success(variables);
        }

        List<HistoricVariableInstance> list =
                historyService.createHistoricVariableInstanceQuery().processInstanceId(paramDto.getExecutionId()).list();

        Map<String, Object> variables=new HashMap<>();
        for (HistoricVariableInstance historicVariableInstance : list) {
            variables.put(historicVariableInstance.getVariableName(),historicVariableInstance.getValue());
        }

        return R.success(variables);


    }



}
