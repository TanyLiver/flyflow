package cc.flyflow.core.utils;

import cc.flyflow.common.dto.*;
import cc.flyflow.common.dto.third.DeptDto;
import cc.flyflow.common.dto.third.MessageDto;
import cc.flyflow.common.dto.third.UserQueryDto;
import cc.flyflow.common.utils.HttpUtil;
import cc.flyflow.common.utils.JsonUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Map;

public class BizHttpUtil {


    public static String post(Object object, String url, String tenantId) {
        Environment environment = SpringUtil.getBean(Environment.class);
        String bizUrl = environment.getProperty("biz.url");

        return HttpUtil.post(object, url, bizUrl, tenantId);

    }


    public static <T> R<List<T>> postArray(Object object, String url, String tenantId) {
        String post = post(object, url, tenantId);

        R<List<T>> r = JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<List<T>>>() {
        });
        return r;
    }

    public static String get(String url, String tenantId) {
        Environment environment = SpringUtil.getBean(Environment.class);
        String bizUrl = environment.getProperty("biz.url");
        return HttpUtil.get(url, bizUrl, tenantId);
    }


    /**
     * 节点开始事件
     *
     * @param nodeRecordParamDto
     */
    public static void startNodeEvent(ProcessInstanceNodeRecordParamDto nodeRecordParamDto) {

        String post = post(nodeRecordParamDto, "/remote/startNodeEvent", nodeRecordParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }


    /**
     * 节点结束事件
     *
     * @param nodeRecordParamDto
     */
    public static void endNodeEvent(ProcessInstanceNodeRecordParamDto nodeRecordParamDto) {
        String post = post(nodeRecordParamDto, "/remote/endNodeEvent", nodeRecordParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }

    /**
     * 节点取消事件
     *
     * @param nodeRecordParamDto
     */
    public static void cancelNodeEvent(ProcessInstanceNodeRecordParamDto nodeRecordParamDto) {
        String post = post(nodeRecordParamDto, "/remote/cancelNodeEvent", nodeRecordParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }

    /**
     * 查询部门列表
     *
     * @param deptIdList
     * @param tenantId
     */
    public static List<DeptDto> queryDeptList(List<String> deptIdList, String tenantId) {
        String post = post(deptIdList, "/remote/queryDeptList", tenantId);
        R<List<DeptDto>> listR = JsonUtil.parseObject(post, new JsonUtil.TypeReference<R<List<DeptDto>>>() {
        });
        return listR.getData();
    }

    /**
     * 保存消息
     *
     * @param messageDto
     */
    public static void saveMessage(MessageDto messageDto) {
        String post = post(messageDto, "/remote/saveMessage", messageDto.getTenantId());
        HttpUtil.checkResult(post);
    }

    /**
     * 流程结束事件
     *
     * @param processInstanceParamDto
     */
    public static void processEndEvent(ProcessInstanceParamDto processInstanceParamDto) {
        String post = post(processInstanceParamDto, "/remote/processEndEvent", processInstanceParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }



    /**
     * 流程开始事件
     *
     * @param processInstanceParamDto
     */
    public static void processStartEvent(ProcessInstanceRecordParamDto processInstanceParamDto) {

        String post = post(processInstanceParamDto, "/remote/processStartEvent", processInstanceParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }

    /**
     * 根据角色id集合查询用户id集合
     *
     * @param roleIdList
     * @param tenantId
     */
    public static R<List<String>> queryUserIdListByRoleIdList(List<String> roleIdList, String tenantId) {
        return postArray(roleIdList, "/remote/queryUserIdListByRoleIdList", tenantId);
    }

    /**
     * 根据角色id集合和部门id集合查询用户id集合
     *
     * @param roleIdList 角色id集合
     * @param deptIdList 部门id集合
     * @param tenantId
     */
    public static R<List<String>> queryUserIdListByRoleIdListAndDeptIdList(List<String> roleIdList,
                                                                           List<String> deptIdList,
                                                                           String tenantId) {

        UserQueryDto userQueryDto = new UserQueryDto();
        userQueryDto.setDeptIdList(deptIdList);
        userQueryDto.setRoleIdList(roleIdList);
        return postArray(userQueryDto, "/remote/queryUserIdListByRoleIdListAndDeptIdList", tenantId);
    }

    /**
     * 根据用户id查询角色id集合
     *
     * @param userId
     * @param tenantId
     */
    public static List<String> loadRoleIdListByUserId(String userId, String tenantId) {
        String s = get("/remote/loadRoleIdListByUserId?userId=" + userId, tenantId);
        return JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<List<String>>>() {
        }).getData();
    }

    /**
     * 根据部门id集合查询所有的用户id集合
     *
     * @param deptIdList
     * @param tenantId
     */
    public static R<List<String>> queryUserIdListByDepIdList(List<String> deptIdList, String tenantId) {
        String s = post(deptIdList, "/remote/queryUserIdListByDepIdList", tenantId);
        R<List<String>> r = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<List<String>>>() {
        });
        return r;
    }

    /**
     * 查询上级部门
     *
     * @param deptId
     * @param tenantId
     */
    public static R<List<DeptDto>> queryParentDeptList(String deptId, String tenantId) {
        String s = get("/remote/queryParentDeptList?deptId=" + deptId, tenantId);
        R<List<DeptDto>> r = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<List<DeptDto>>>() {
        });
        return r;
    }

    /**
     * 批量查询子级部门
     *
     * @param deptIdList
     * @param tenantId
     */
    public static R<Map<String, List<DeptDto>>> batchQueryChildDeptList(List<String> deptIdList, String tenantId) {
        String s = post(deptIdList, "/remote/batchQueryChildDeptList", tenantId);
        R<Map<String, List<DeptDto>>> r = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<Map<String,
                List<DeptDto>>>>() {
        });
        return r;
    }


    /**
     * 查询流程管理员
     *
     * @param flowId   流程id
     * @param tenantId
     */
    public static R<String> queryProcessAdmin(String flowId, String tenantId) {
        String s = get("/remote/queryProcessAdmin?flowId=" + flowId + "&tenantId=" + tenantId, tenantId);
        R<String> longR = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<String>>() {
        });
        return longR;
    }



    /**
     * 查询流程
     *
     * @param flowId   流程id
     * @param tenantId
     */
    public static R<ProcessDto> queryProcess(String flowId,String tenantId) {
        String s = get("/remote/queryProcess?flowId=" + flowId+"&tenantId="+tenantId, tenantId);
        R<ProcessDto> longR = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<ProcessDto>>() {
        });
        return longR;
    }

    /**
     * 查询节点数据
     *
     * @param flowId
     * @param nodeId
     * @param tenantId
     * @return
     */
    public static R<String> queryNodeOriData(String flowId, String nodeId,String tenantId) {
        String s = get(StrUtil.format("/remote/getNodeData?flowId" +
                "={}&nodeId={}&tenantId={}", flowId, nodeId,tenantId), tenantId);
        R<String> r = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<String>>() {
        });

        return r;
    }

    /**
     * 查询所有的用户信息
     *
     * @param userId
     * @param tenantId
     * @return
     */
    public static R<Map<String, Object>> queryUserInfo(String userId,String tenantId) {
        String s = get("/remote/queryUserAllInfo?userId=" + userId, tenantId);
        R<Map<String, Object>> mapR = JsonUtil.parseObject(s, new JsonUtil.TypeReference<R<Map<String, Object>>>() {
        });
        return mapR;
    }


    /**
     * 节点开始指派用户了
     *
     * @param processInstanceAssignUserRecordParamDto
     * @return
     */
    public static void createTaskEvent(ProcessInstanceAssignUserRecordParamDto processInstanceAssignUserRecordParamDto) {
        String post = post(processInstanceAssignUserRecordParamDto, "/remote/createTaskEvent",
                processInstanceAssignUserRecordParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }

    /**
     * 任务完成事件
     *
     * @param processInstanceAssignUserRecordParamDto
     * @return
     */
    public static void taskCompletedEvent(ProcessInstanceAssignUserRecordParamDto processInstanceAssignUserRecordParamDto) {
        String post = post(processInstanceAssignUserRecordParamDto, "/remote/taskCompletedEvent",
                processInstanceAssignUserRecordParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }

    /**
     * 任务结束事件
     *
     * @param processInstanceAssignUserRecordParamDto
     * @return
     */
    public static void taskEndEvent(ProcessInstanceAssignUserRecordParamDto processInstanceAssignUserRecordParamDto) {
        String post = post(processInstanceAssignUserRecordParamDto, "/remote/taskEndEvent",
                processInstanceAssignUserRecordParamDto.getTenantId());
        HttpUtil.checkResult(post);
    }


    /**
     * 保存抄送数据
     *
     * @param processCopyDto
     * @return
     */
    public static void saveCC(ProcessInstanceCopyDto processCopyDto) {
        String post = post(processCopyDto, "/remote/saveCC", processCopyDto.getTenantId());
        HttpUtil.checkResult(post);

    }

    /**
     * 保存节点原始数据
     *
     * @param processNodeDataDto
     * @return
     */
    public static void saveNodeOriData(ProcessNodeDataDto processNodeDataDto) {
        String post = post(processNodeDataDto, "/remote/saveNodeData", processNodeDataDto.getTenantId());
        HttpUtil.checkResult(post);
    }


    /**
     * 保存执行信息
     *
     * @param processInstanceExecutionDto
     * @return
     */
    public static void saveExecution(ProcessInstanceExecutionDto processInstanceExecutionDto) {
        String post = post(processInstanceExecutionDto, "/remote/saveExecution",
                processInstanceExecutionDto.getTenantId());
        HttpUtil.checkResult(post);
    }


}
