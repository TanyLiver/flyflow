package cc.flyflow.core.service;

import cc.flyflow.common.dto.ClearProcessParamDto;
import cc.flyflow.common.dto.CreateFlowDto;
import cc.flyflow.common.dto.ProcessInstanceParamDto;
import cc.flyflow.common.dto.R;

/**
 * @author Huijun Zhao
 * @description
 * @date 2023-08-04 16:40
 */
public interface IFlowService {

    /**
     * 创建流程模型
     * @param createFlowDto
     * @return
     */
    R create(  CreateFlowDto createFlowDto);

    /**
     * 发起流程
     * @param processInstanceParamDto
     * @return
     */
    R start(  ProcessInstanceParamDto processInstanceParamDto);


    /**
     * 清理所有的流程
     * @param clearProcessParamDto 清理数据对象
     * @return 成功失败
     */
    R clearProcess(ClearProcessParamDto clearProcessParamDto);

}
